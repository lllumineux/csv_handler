import csv
import statistics
import sys
import datetime


DATETIME_RUS_PATTERN = "%d.%m.%Y, %H:%M:%S"
DATETIME_ENG_PATTERN = "%-m/%-d/%Y, %I:%M:%S %p"
SESSION_OFFSET = 600  # in seconds
NEW_LINE_SYM = "\n"


class TeamsSession:
    def __init__(self, csv_file_name):
        self.info = {}

        with open(csv_file_name, encoding="utf-16") as csv_file:
            csv_file_content = csv.reader(csv_file, delimiter="\t")
            rows = [row for row in csv_file_content]
            info_table = rows[-int(rows[1][1])::]

            for row in info_table:
                time_joined = datetime.datetime.strptime(row[1], DATETIME_RUS_PATTERN)
                time_left = datetime.datetime.strptime(row[2], DATETIME_RUS_PATTERN)

                if row[0] in self.info:
                    self.info[row[0]][1] = time_left
                else:
                    self.info[row[0]] = [time_joined, time_left, datetime.timedelta(0, 0)]

            self.median_start_time = datetime.datetime.fromtimestamp(
                statistics.median(
                    [self.info[name][0].timestamp() for name in self.info]
                ) - SESSION_OFFSET
            )
            self.median_end_time = datetime.datetime.fromtimestamp(
                statistics.median(
                    [self.info[name][1].timestamp() for name in self.info]
                ) + SESSION_OFFSET
            )

            for row in info_table:
                time_joined = datetime.datetime.strptime(row[1], DATETIME_RUS_PATTERN)
                time_left = datetime.datetime.strptime(row[2], DATETIME_RUS_PATTERN)

                if time_joined < self.median_start_time and time_left < self.median_start_time:
                    pass
                elif time_joined < self.median_start_time and time_left < self.median_end_time:
                    self.info[row[0]][2] += time_left - self.median_start_time
                elif time_joined >= self.median_start_time and time_left < self.median_end_time:
                    self.info[row[0]][2] += time_left - time_joined
                elif time_joined >= self.median_start_time and time_left >= self.median_end_time:
                    self.info[row[0]][2] += self.median_end_time - time_joined
                elif time_joined >= self.median_end_time and time_left >= self.median_end_time:
                    pass
                elif time_joined < self.median_start_time and time_left > self.median_end_time:
                    self.info[row[0]][2] += self.median_end_time - self.median_start_time

    def get_top_participants(self, n, is_reverse: bool = True):
        return list(sorted(self.info, key=lambda x: self.info[x][2], reverse=is_reverse))[:n]

    def get_often_participants(self, percent):
        return list(
            filter(
                lambda x: self.info[x][2].seconds / (
                        self.median_end_time - self.median_start_time
                ).seconds >= percent / 100,
                self.info
            )
        )


def main():
    teams_session = TeamsSession(sys.argv[1])
    print(f"Топ-3:\n{NEW_LINE_SYM.join([name for name in teams_session.get_top_participants(3)])}")
    print()
    print(f"Кто присутствовал более 80% пары:\n{NEW_LINE_SYM.join(teams_session.get_often_participants(10))}")


if __name__ == "__main__":
    main()
